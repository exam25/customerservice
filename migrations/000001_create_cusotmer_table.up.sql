CREATE TABLE IF NOT EXISTS customer(
    customer_id serial PRIMARY KEY NOT NULL,
    firstname VARCHAR(30)not null,
    lastname VARCHAR(30)NOT NULL,
    bio TEXT,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    username VARCHAR(30),
    phonenumber VARCHAR(30),
    refreshtoken TEXT,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP);
