package service

import (
	"context"
	"fmt"

	pb "github/FIrstService/exammicro/customerservice/genproto/customer"
	"github/FIrstService/exammicro/customerservice/genproto/post"

	// "github/FIrstService/exammicro/customerservice/genproto/review"

	l "github/FIrstService/exammicro/customerservice/pkg/logger"
	grpcclient "github/FIrstService/exammicro/customerservice/service/grpc_client"
	"github/FIrstService/exammicro/customerservice/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// CustomerService ...
type CustomerService struct {
	post *grpcclient.ServiceManager
	// review  *grpcclient.ServiceManager
	storage storage.IStorage
	logger  l.Logger
}

// NewCustomerService ...

func NewCustomerService(post *grpcclient.ServiceManager, db *sqlx.DB, log l.Logger) *CustomerService {
	return &CustomerService{
		post:    post,
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

func (s *CustomerService) CreateCustomer(ctx context.Context, req *pb.CustomerReq) (*pb.CustomerResp, error) {
	user, err := s.storage.Customer().CreateCustomer(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error insert customer", err))
		return &pb.CustomerResp{}, status.Error(codes.Internal, "something went wrong, please check customer info")
	}
	return user, nil
}

func (s *CustomerService) GetCustomerById(ctx context.Context, req *pb.ID) (*pb.Customer, error) {
	res, err := s.storage.Customer().GetCustomerById(req)
	if err != nil {
		s.logger.Error("Error while getting", l.Any("error select users", err))
		return &pb.Customer{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}

	response := &pb.Customer{
		CustomerId:  res.CustomerId,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		Bio:         res.Bio,
		Email:       res.Email,
		PhoneNumber: res.PhoneNumber,
		Addresses:   res.Addresses,
	}

	return response, nil
}

func (s *CustomerService) UpdateCustomer(ctx context.Context, req *pb.Customer) (*pb.Customer, error) {
	res, err := s.storage.Customer().UpdateCustomer(req)
	if err != nil {
		s.logger.Error("Error while updating", l.Any("Update", err))
		return &pb.Customer{}, status.Error(codes.InvalidArgument, "Please check customer info")
	}
	return res, nil

}

func (s *CustomerService) DeleteCustomer(ctx context.Context, req *pb.ID) (*pb.Empty, error) {
	fmt.Println(req.Id)
	err := s.storage.Customer().DeleteCustomer(req)
	if err != nil {
		s.logger.Error("Error while delete customer", l.Any("Delete", err))
		return nil, status.Error(codes.InvalidArgument, "wrong id for delete")
	}

	_, err = s.post.PostService().DeleteByCustId(ctx, &post.ID{Id: req.Id})
	if err != nil {
		s.logger.Error("Error while delete post", l.Any("Delete", err))
		return nil, status.Error(codes.InvalidArgument, "wrong id for delete")
	}

	// _, err = s.post.ReviewService().DeleteByCustomerId(ctx, &review.ID{Id: req.Id})
	// if err != nil {
	// 	s.logger.Error("Error while deleting customer posts", l.Error(err))
	// 	return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	// }
	return &pb.Empty{}, nil
}

func (s *CustomerService) GetCustomerAllinfo(ctx context.Context, req *pb.ID) (*pb.GetCustomer, error) {
	res, err := s.storage.Customer().GetCustomerById(req)
	fmt.Println(res)
	if err != nil {
		s.logger.Error("Error while getting customer info", l.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}
	posts, err := s.post.PostService().GetByCustId(ctx, &post.ID{Id: res.CustomerId})
	// fmt.Println(posts)
	if err != nil {
		s.logger.Error("Error while getting customer posts", l.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}

	for _, i := range posts.Posts {
		query := &pb.GetPost{
			PostId:      i.PostId,
			CustomerId:  res.CustomerId,
			Description: i.Description,
			Name:        i.Name,
		}
		for _, media := range i.Medias {
			query.Medias = append(query.Medias, (*pb.MediaResp)(media))
		}
		for _, r := range i.Rewiews {
			query.Review = append(query.Review, (*pb.Review)(r))
		}
		res.Posts = append(res.Posts, query)
	}
	res.CountPosts = posts.Count
	return res, nil
}

func (s *CustomerService) CheckField(ctx context.Context, req *pb.CheckFieldReq) (*pb.CheckFieldResp, error) {
	user, err := s.storage.Customer().CheckField(req)
	if err != nil {
		s.logger.Error("Error while chekking", l.Any("error check email", err))
		return &pb.CheckFieldResp{}, status.Error(codes.Internal, "something went wrong, please check user email")
	}
	return user, nil
}

func (s *CustomerService) GetLists(ctx context.Context, req *pb.ListReq) (*pb.ListResp, error) {
	res, err := s.storage.Customer().GetLists(req)
	if err != nil {
		s.logger.Error("Error while getting", l.Any("error select users", err))
		return &pb.ListResp{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *CustomerService) CreateUser(ctx context.Context, req *pb.User) (*pb.User, error) {
	user, err := s.storage.Customer().CreateUser(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error insert customer", err))
		return &pb.User{}, status.Error(codes.Internal, "something went wrong, please check customer info")
	}
	return user, nil
}

func (s *CustomerService) GetByEmail(ctx context.Context, req *pb.LoginReq) (*pb.User, error) {
	res, err := s.storage.Customer().GetByEmail(req)
	if err != nil {
		s.logger.Error("Error while get email", l.Any("GetByEmail", err))
		return &pb.User{}, status.Error(codes.InvalidArgument, "Please check customer info")
	}
	return res, nil

}

func (s *CustomerService) GetAdminByUsername(ctx context.Context, req *pb.AdminReq) (*pb.AdminResp, error) {
	res, err := s.storage.Customer().GetAdminByUsername(req)
	if err != nil {
		s.logger.Error("Error while get email", l.Any("GetByEmail", err))
		return &pb.AdminResp{}, status.Error(codes.InvalidArgument, "Please check customer info")
	}
	return res, nil

}

func (s *CustomerService) GetModeratorByUsername(ctx context.Context, req *pb.ModeratorReq) (*pb.ModeratorResp, error) {
	res, err := s.storage.Customer().GetModeratorByUsername(req)
	if err != nil {
		s.logger.Error("Error while get email", l.Any("GetByEmail", err))
		return &pb.ModeratorResp{}, status.Error(codes.InvalidArgument, "Please check customer info")
	}
	return res, nil

}
