package grpcClient

import (
	"fmt"
	"github/FIrstService/exammicro/customerservice/config"
	pbp "github/FIrstService/exammicro/customerservice/genproto/post"
	pbr "github/FIrstService/exammicro/customerservice/genproto/review"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManager struct {
	conf          config.Config
	postservice   pbp.PostServiceClient
	reviewservice pbr.ReviewServiceClient
}

func New(cnfg config.Config) (*ServiceManager, error) {
	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cnfg.PostServiceHost, cnfg.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial product service: host: %s and port: %d",
			cnfg.PostServiceHost, cnfg.PostServicePort)

	}

	connReview, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cnfg.PostServiceHost, cnfg.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial product service: host: %s and port: %d",
			cnfg.PostServiceHost, cnfg.PostServicePort)

	}

	serviceManager := &ServiceManager{
		conf:          cnfg,
		postservice:   pbp.NewPostServiceClient(connPost),
		reviewservice: pbr.NewReviewServiceClient(connReview),
	}

	return serviceManager, nil
}

func (s *ServiceManager) PostService() pbp.PostServiceClient {
	return s.postservice
}

func (s *ServiceManager) ReviewService() pbr.ReviewServiceClient {
	return s.reviewservice
}
