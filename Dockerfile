FROM golang:1.19-alpine
RUN mkdir customer
COPY . /customer
WORKDIR /customer
RUN go mod tidy
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 9001